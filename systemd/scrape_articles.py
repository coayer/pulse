import logging
import requests
import html2text
import sqlite3
import argparse
from time import sleep
from random import shuffle
from os import path
from datetime import datetime
from urllib.parse import urlparse
from facebook_scraper import get_posts, exceptions
from readability import Document


def init_database():
    cursor.execute("""CREATE TABLE Outlets (
            OutletID INTEGER PRIMARY KEY AUTOINCREMENT,
            Domain TEXT,
            UNIQUE(Domain)
        )""")

    cursor.execute("""CREATE TABLE Articles (
        ArticleID INTEGER PRIMARY KEY AUTOINCREMENT, 
        OutletID INTEGER REFERENCES Outlets (OutletID),
        Title TEXT,
        Content TEXT,
        FetchedTime TEXT,
        URL TEXT,
        UNIQUE(URL)
    )""")

    cursor.execute("""CREATE TABLE SocialMedia (
        SocialMediaID INTEGER PRIMARY KEY AUTOINCREMENT,
        Name TEXT,
        UNIQUE(Name)
    )""")

    cursor.execute("""CREATE TABLE Posts (
        PostID INTEGER PRIMARY KEY AUTOINCREMENT,
        ArticleID INTEGER REFERENCES Articles (ArticleID),
        SocialMediaID INTEGER REFERENCES SocialMedia (SocialMediaID),
        UNIQUE(ArticleID, SocialMediaID)
    )""")  # schema could handle the same link from two different social media platforms

    cursor.execute("""CREATE TABLE PostDatatypes (
        TypeID INTEGER PRIMARY KEY AUTOINCREMENT,
        Name TEXT,
        UNIQUE(Name)
    )""")

    cursor.execute("""CREATE TABLE PostDetails (
        PostID INTEGER REFERENCES Posts (PostID),
        TypeID INTEGER REFERENCES PostDatatypes (TypeID),
        Value TEXT,
        PRIMARY KEY(PostID, TypeID)
    )""")

    db_conn.commit()


def get_article(url):
    logging.info("Requesting " + url)
    try:
        response = requests.get(url, headers=HEADERS, timeout=TIMEOUT)
    except requests.exceptions.Timeout:
        logging.debug("Timeout for " + url)
        return None, None

    if response.status_code != 200:
        logging.debug("Received " + str(response.status_code) + " from " + url)
        return None, None

    doc = Document(response.text)

    text_maker = html2text.HTML2Text()
    text_maker.ignore_links = True
    text_maker.ignore_emphasis = True
    text_maker.ignore_images = True
    text_maker.ignore_tables = True

    return doc.title(), text_maker.handle(doc.summary())


def facebook_articles_scrape():
    with open(WORK_DIR + "facebook_outlets.txt") as fb_outlets_file:  # pages to scrape
        facebook_outlets = fb_outlets_file.read().splitlines()
        shuffle(facebook_outlets)  # mitigates temp bans

    fb_datatypes = ["fb_comments", "fb_post_id", "fb_time", "fb_shares", "fb_link", "fb_fetched_time", "fb_post_text"]
    # reactions are returned in dictionary from scraper so need to be handled separately
    fb_reactions = ["fb_wow", "fb_haha", "fb_like", "fb_sad", "fb_care", "fb_angry", "fb_love"]

    cursor.execute("INSERT OR IGNORE INTO SocialMedia (Name) VALUES ('facebook')")
    cursor.executemany("INSERT OR IGNORE INTO PostDatatypes (Name) VALUES (?)", [(field,) for field in fb_datatypes])
    cursor.executemany("INSERT OR IGNORE INTO PostDatatypes (Name) VALUES (?)", [(field,) for field in fb_reactions])

    db_conn.commit()

    # will reuse the ID so prevents many queries
    facebook_id = cursor.execute("SELECT SocialMediaID FROM SocialMedia WHERE Name='facebook'").fetchone()[0]

    for outlet in facebook_outlets:
        try:
            # cookies & extra info needed for reactions
            for post in get_posts(outlet, extra_info=True, cookies=WORK_DIR + "facebook.com_cookies.txt", pages=6):
                if post["link"] is None or post["reactions"] is None:
                    continue

                article_title, article_content = get_article(post["link"])
                if article_content is None:  # non 200 response code or timeout, will stop trying with this outlet
                    break

                # adds news outlet to DB
                domain = urlparse(post["link"]).netloc
                cursor.execute("INSERT OR IGNORE INTO Outlets (Domain) VALUES (?)", (domain,))

                # adds article to DB
                cursor.execute(
                    "INSERT OR IGNORE INTO Articles (OutletID, Title, Content, FetchedTime, URL) VALUES"  # will only fetch article once due to unique URL
                    "((SELECT OutletID FROM Outlets WHERE Outlets.Domain=?), ?, ?, ?, ?)",
                    (domain, article_title, article_content, datetime.now(), post["link"]))

                article_id = cursor.execute("SELECT ArticleID FROM Articles WHERE Articles.URL=?", (post["link"],)).fetchone()[0]

                # adds post to DB
                cursor.execute("INSERT OR IGNORE INTO Posts (ArticleID, SocialMediaID) VALUES (?, ?)",
                               (article_id, facebook_id))

                # gets post ID
                post_id = cursor.execute("SELECT PostID FROM Posts WHERE ArticleID=? AND SocialMediaID=?",
                                         (article_id, facebook_id)).fetchone()[0]

                # adds/updates post information to DB
                for field in fb_datatypes:
                    datatype_id = cursor.execute("SELECT TypeID FROM PostDatatypes WHERE Name=?", (field,)).fetchone()[0]
                    value = str(post[field[3:]])
                    cursor.execute("INSERT INTO PostDetails (PostID, TypeID, Value) VALUES (?, ?, ?) "
                                   "ON CONFLICT (PostID, TypeID) DO UPDATE SET Value=?",
                                   (post_id, datatype_id, value, value))

                # adds/updates post reactions to DB
                for reaction in fb_reactions:
                    datatype_id = cursor.execute("SELECT TypeID FROM PostDatatypes WHERE Name=?", (reaction,)).fetchone()[0]
                    if reaction[3:] in post["reactions"]:
                        value = post["reactions"][reaction[3:]]
                        cursor.execute("INSERT INTO PostDetails (PostID, TypeID, Value) VALUES (?, ?, ?) "
                                       "ON CONFLICT (PostID, TypeID) DO UPDATE SET Value=?",
                                       (post_id, datatype_id, value, value))

                db_conn.commit()

        except exceptions.TemporarilyBanned:
            logging.error("Temp banned")
            sleep(1200)


parser = argparse.ArgumentParser()
parser.add_argument("path", help="path of working directory, use ./ for here")
args = parser.parse_args()

TIMEOUT = 10

HEADERS = {
    "user-agent": "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) "
                  "Chrome/91.0.4472.114 Safari/537.36",
}

WORK_DIR = args.path
DB_PATH = WORK_DIR + "articles.db"

logging.basicConfig(level=logging.INFO)

if not path.isfile(DB_PATH):
    db_conn = sqlite3.connect(DB_PATH)
    cursor = db_conn.cursor()
    init_database()
else:
    db_conn = sqlite3.connect(DB_PATH)
    cursor = db_conn.cursor()

facebook_articles_scrape()
