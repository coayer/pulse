import spacy
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from joblib import dump
import pandas as pd

nlp = spacy.load("en_core_web_sm")
data = pd.read_csv("news_positivity.csv")

def clean(text):
    document = nlp(text)
    return [token.lemma_.lower() for token in document if (not token.is_stop and token.is_alpha)]

vectorizer = TfidfVectorizer(tokenizer=clean)   # a lot slower than the default

X = vectorizer.fit_transform(data["Text"])
y = data["Positivity"]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1)
model = LogisticRegression().fit(X_train, y_train)

dump(model, "../qary/src/qary/data/models/pulse/classifier.joblib")
dump(vectorizer, "../qary/src/qary/data/models/pulse/vectorizer.joblib")
