import logging
import csv
from sys import exit
import pulse_etl
from joblib import load, dump
from time import time
from os import system, path
# from sklearn.multioutput import MultiOutputRegressor
from sklearn.linear_model import SGDClassifier
# from sklearn.feature_extraction.text import TfidfVectorizer
from numpy import argmax, mean, append

NUMBER_STORIES = 25
MODEL_DIRECTORY = "models/"

def get_user_click(stories_count):
    while True:
        user_input = input("Select article to read (q to quit): ")
        try:
            article_selection = int(user_input)
            if 0 < article_selection <= stories_count:
                return article_selection - 1
        except ValueError:
            if user_input == 'q':
                return -1
            else:
                print("Must enter index in range", min, stories_count)


def show_news(stories):
    print()
    print("_______________________________________________________________________________")
    print()
    print("CURRENT STORIES")
    print()

    for story_index in range(len(stories)):
        print(story_index + 1, stories[story_index]["title"])

    print()


def article_inference(article):
    global fb_vectorizer, fb_model, fb_reactions_scaler  # other social media platforms go here
    return fb_reactions_scaler.transform(fb_model.predict(fb_vectorizer.transform([article])))


def score_article(article):
    global user_model
    return user_model.predict_proba(article_inference(article))[0][1]


def adjust_user_model_weights(article, reading_time):
    global user_model

    article_vector = article_inference(article)

    if reading_time < (len(article) / 22) / 2:  # using 22 characters/sec reading speed, less than half predicted time
        user_model.partial_fit(article_vector, [0])
    else:
        user_model.partial_fit(article_vector, [1])

    has_header = path.isfile("article_score_history.csv")
    with open("article_score_history.csv", "a+") as history_csv:
        writer = csv.writer(history_csv)
        if not has_header:
            writer.writerow(["fb_wow", "fb_haha", "fb_like", "fb_sad", "fb_care", "fb_angry", "fb_love", "reading_time"])
        writer.writerow(append(article_vector, [reading_time]))

    dump(user_model, MODEL_DIRECTORY + "user_model.joblib")


print("Loading models...")
fb_model = load(MODEL_DIRECTORY + "fb_content_regressor.joblib")
fb_vectorizer = load(MODEL_DIRECTORY + "fb_vectorizer.joblib")
fb_reactions_scaler = load(MODEL_DIRECTORY + "fb_reactions_scaler.joblib")

if not path.isfile(MODEL_DIRECTORY + "user_model.joblib"):
    user_model = SGDClassifier(random_state=3, warm_start=True, loss="log")
    user_model.fit([[0, 0.25, 0.75, 0, 0.5, 0, 1], [0.5, 0.1, 0.5, 1, 0.1, 1, 0]], [1, 0])
else:
    user_model = load(MODEL_DIRECTORY + "user_model.joblib")

print("Fetching news...")
stories = pulse_etl.get_news_stories_text()

print("Analyzing news stories...")
for story in stories:
    story["scores"] = [score_article(article) for article in story["articles"]]

# sometimes scraper fails to get page, need to give empty articles 0 score
stories.sort(key=lambda story: mean(story["scores"]) if len(story["scores"]) > 0 else 0, reverse=True)
stories = stories[:NUMBER_STORIES]

while True:
    show_news(stories)
    story_selection = get_user_click(len(stories))

    if story_selection == -1:
        exit(0)

    system("clear")
    print()
    print("_______________________________________________________________________________")
    print()

    start_reading_time = time()
    article = stories[story_selection]["articles"][argmax(stories[story_selection]["scores"])]  # highest scored article

    print(article)
    print()
    input("Press any key to continue")

    stop_reading_time = time()
    time_elapsed = stop_reading_time - start_reading_time

    adjust_user_model_weights(article, time_elapsed)
    system("clear")
