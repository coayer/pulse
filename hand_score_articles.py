import sqlite3
import argparse
from sys import exit
from os import path

parser = argparse.ArgumentParser()
parser.add_argument("path", help="path of working directory, use ./ for here")
args = parser.parse_args()

WORK_DIR = args.path
DB_PATH = WORK_DIR + "articles.db"

if not path.isfile(DB_PATH):
    print("Need existing articles database to run this program")
    exit(1)

db_conn = sqlite3.connect(DB_PATH)
cursor = db_conn.cursor()

human_datatypes = ["informative", "positive", "significant", "biased"]

cursor.execute("INSERT OR IGNORE INTO SocialMedia (Name) VALUES ('human')")
cursor.executemany("INSERT OR IGNORE INTO PostDatatypes (Name) VALUES (?)", [(field,) for field in human_datatypes])

db_conn.commit()

human_id = cursor.execute("SELECT SocialMediaID FROM SocialMedia WHERE Name='human'").fetchone()[0]


def get_score(category):
    while True:
        try:
            score = float(input(category + ": "))
            if 0 <= score <= 1:
                return score
        except ValueError:
            print("Must enter float between 0 and 1")


def get_scores():
    while True:
        scores = [get_score(category) for category in human_datatypes]
        print()
        print(scores)
        confirm = input("Write scores to db? [y/N]\n")
        if confirm == "y":
            return scores


while True:
    article = cursor.execute(
        "SELECT * FROM Articles a "
        "WHERE NOT EXISTS (SELECT 1 FROM Posts WHERE Posts.ArticleID=a.ArticleID AND Posts.SocialMediaID=?) "
        "ORDER BY RANDOM() LIMIT 1", (human_id, )).fetchone()

    if article is None:
        print("No more articles to score!")
        exit(0)

    article_id = article[0]

    print()
    print()
    print("_______________________________________________________________________________")
    print()
    print(article[2].split(" - ")[0])
    print()
    print()
    print(article[3])

    skip = input("Skip article? [y/N]\n")
    if skip == "y":
        continue

    scores = get_scores()

    # adds post to DB
    cursor.execute("INSERT INTO Posts (ArticleID, SocialMediaID) VALUES (?, ?)",
                   (article_id, human_id))

    # gets post ID
    post_id = cursor.execute("SELECT PostID FROM Posts WHERE ArticleID=? AND SocialMediaID=?",
                             (article_id, human_id)).fetchone()[0]

    # adds scores to DB
    for i in range(len(human_datatypes)):
        datatype_id = cursor.execute("SELECT TypeID FROM PostDatatypes WHERE Name=?", (human_datatypes[i],)).fetchone()[0]
        value = str(scores[i])
        cursor.execute("INSERT INTO PostDetails (PostID, TypeID, Value) VALUES (?, ?, ?)", (post_id, datatype_id, value))

    db_conn.commit()
